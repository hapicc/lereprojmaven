/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lere.dao;

import br.com.lere.model.Musica;
import br.com.lere.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Hanna
 */
public class MusicaDao implements InterfaceMusica {

    @Override
    public Musica getMusica(int id) {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Musica mus = (Musica) ss.load(Musica.class, id);
        HibernateUtil.close(ss);
        return mus;
    }

    @Override
    public void inserir(Musica musica) {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            ss.save(musica);
            ts.commit();
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
             HibernateUtil.close(ss);
        }
    }

    @Override
    public void alterar(Musica musica) {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            ss.update(musica);
            ts.commit();
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
             HibernateUtil.close(ss);
        }
    }

    @Override
    public void remover(Musica musica) {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            ss.delete(musica);
            ts.commit();
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
             HibernateUtil.close(ss);
        }
    }

    @Override
    public List<Musica> list() {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            List lista = ss.createQuery("From musica ").list();
            ts.commit();
            return lista;
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
           HibernateUtil.close(ss);
        }
    }

    @Override
    public List<Musica> listNome() {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            List lista = ss.createQuery("From musica ORDER BY nome").list();
            ts.commit();
            return lista;
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
             HibernateUtil.close(ss);
        }
    }

    @Override
    public List<Musica> listNivel() {
        Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            List lista = ss.createQuery("From musica ORDER BY nivel").list();
            ts.commit();
            return lista;
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
             HibernateUtil.close(ss);
        }
    }

    @Override
    public Musica findById(int id) {
         Session ss = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = ss.beginTransaction();
        try {
            Musica musica = (Musica)ss.createQuery("From musica where id="+id).list();
            ts.commit();
            return musica;
        } catch (Exception ex) {
            ts.rollback();
            throw ex;
        } finally {
           HibernateUtil.close(ss);
        }
    }
    
    

}
