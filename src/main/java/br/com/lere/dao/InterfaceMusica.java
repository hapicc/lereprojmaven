
package br.com.lere.dao;

import br.com.lere.model.Musica;
import java.util.List;

public interface InterfaceMusica {
    
    public Musica getMusica(int id);
    
    public void inserir(Musica musica);
    
    public void alterar(Musica musica);
    
    public void remover(Musica musica);
    
    public List<Musica> list();
    
    public List<Musica> listNome();
    
    public List<Musica> listNivel();
    
    public Musica findById(int id);
}
