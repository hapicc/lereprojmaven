/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lere.controller;

import br.com.lere.dao.InterfaceMusica;
import br.com.lere.dao.MusicaDao;
import br.com.lere.model.Musica;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Hanna
 */
@ManagedBean
@SessionScoped
public class MusicaBean implements Serializable {

    private String partitura;
    private Musica musica;
    private String erro = null;
    private transient DataModel<Musica> listaMusica;
    private transient DataModel<Musica> listaNivel;

    public void existe(String caminho) {
        File file = new File(caminho);
        if (!file.exists()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", "Arquivo não existe!"));
        }
    }

    public void prepararAdicionarMusica(ActionEvent actionEvent) {
        musica = new Musica();
    }

    public void prepararMusica(ActionEvent actionEvent) {
        musica = (Musica) (listaMusica.getRowData());
    }

    public String adicionar(ActionEvent actionEvent) throws URISyntaxException, IOException {
        try {

            File upload = new File(musica.getCaminhoxml());
            String requestURL = "http://localhost:8084/LereMaven/rest/file/upload";

            HttpResponse<com.mashape.unirest.http.JsonNode> jsonResponse = Unirest.post(requestURL)
                    .header("accept", "application/json")
                    .field("file", upload)
                    .asJson();
            ObjectMapper mapper = new ObjectMapper();
            Musica jsonObj = mapper.readValue(jsonResponse.getBody().toString(), Musica.class);
            musica.setCaminhomidi(jsonObj.getCaminhomidi());
            musica.setCaminhoxml(jsonObj.getCaminhoxml());
            musica.setNivel(jsonObj.getNivel());

        } catch (Exception ex) {
            Logger.getLogger(MusicaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (musica.getNivel() < 1) {
            erro = "Erro ao classificar arquivo";
            return "erro";
        }
        InterfaceMusica dao = new MusicaDao();
        dao.inserir(musica);
        return "resultado";
    }

    public String alterar(ActionEvent actionEvent) {
        InterfaceMusica dao = new MusicaDao();

        dao.alterar(musica);
        return "musica";
    }

    public String excluir(ActionEvent actionEvent) {
        musica = (Musica) (listaMusica.getRowData());
        InterfaceMusica dao = new MusicaDao();
        File f;
        if (musica.getCaminhomidi() != null) {
            f = new File(musica.getCaminhomidi());
            f.delete();
        }
        if (musica.getCaminhoxml() != null) {
            f = new File(musica.getCaminhoxml());
            f.delete();
        }
        dao.remover(musica);
        return "musica";
    }

    public String visualizarPartitura(ActionEvent actionEvent) throws IOException {
        musica = (Musica) (listaMusica.getRowData());
        
        File xmlFile = new File(musica.getCaminhoxml());
        partitura = xmlFile.getName();
                
        File partituraFile = new File("C:\\Users\\Hanna\\Dropbox\\LereProjMaven\\src\\main\\webapp\\"
                +partitura);
        
        copy(xmlFile, partituraFile);
     
        return "partitura";
    }

    public Musica getMusica() {
        return musica;
    }

    public void setMusica(Musica musica) {
        this.musica = musica;
    }

    public DataModel<Musica> getListaMusica() {
        List<Musica> lista = new MusicaDao().listNome();
        listaMusica = new ListDataModel(lista);
        return listaMusica;
    }

    public void setListaMusica(DataModel<Musica> listaMusica) {
        this.listaMusica = listaMusica;
    }

    public DataModel<Musica> getListaNivel() {
        List<Musica> lista = new MusicaDao().listNivel();
        listaNivel = new ListDataModel(lista);
        return listaNivel;
    }

    public void setListaNivel(DataModel<Musica> listaNivel) {
        this.listaNivel = listaNivel;
    }

    public String getErro() {
        return erro;
    }

    public String getPartitura() {
        return partitura;
    }

    public void copy(File source, File destination) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }

}
