/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lere.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Hanna
 */
@Entity(name="musica")
public class Musica implements Serializable {
    
    @Id
    @GeneratedValue
    private int id;
    private String nome;
    private int nivel;
    private String caminhoxml;
    private String autor;
    private String ano;
    private String genero;
    private String caminhomidi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getCaminhoxml() {
        return caminhoxml;
    }

    public void setCaminhoxml(String caminhoxml) {
        this.caminhoxml = caminhoxml;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCaminhomidi() {
        return caminhomidi;
    }

    public void setCaminhomidi(String caminhomidi) {
        this.caminhomidi = caminhomidi;
    }
    
    
    
}
